import {Inject, Injectable} from '@angular/core';
import {MarkerService} from './marker.service';
import {BehaviorSubject, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MenuService {

    private _openSubject: Subject<boolean> = new Subject<boolean>();
    private _open: boolean = true;
    private _small: boolean;

    constructor(@Inject(MarkerService) private markerService: MarkerService) {
    }
    


    get small(): boolean {
        return this._small;
    }

    set small(value: boolean) {
        if (!this.markerService.enabledPopup) {
            this.markerService.enabledPopup = !value;
        }
        this._small = value;
    }

    get open(): boolean {
        return this._open;
    }

    set open(value: boolean) {
        this.openSubject.next(value);
        this._open = value;
    }

    get openSubject(): Subject<boolean> {
        return this._openSubject;
    }

    set openSubject(value: Subject<boolean>) {
        this._openSubject = value;
    }
}
