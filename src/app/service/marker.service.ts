import {Inject, Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import * as L from 'leaflet';
import {MyMarker} from '../class/MyMarker';
import 'leaflet.markercluster';
import 'leaflet-routing-machine';
import {BrowserStorageService} from './browser-storage.service';


@Injectable({
    providedIn: 'root'
})
export class MarkerService {

    private _map: L.Map;
    private _mapGroup: L.MarkerClusterGroup;
    private _currentMapRouter: L.Routing.Control = null;
    private _list: Array<MyMarker> = [];
    private _setRoutingMarker: Subject<MyMarker> = new Subject<MyMarker>();
    private _setMarker: Subject<MyMarker> = new Subject<MyMarker>();
    private _goToMarker: Subject<MyMarker> = new Subject<MyMarker>();
    private _reload: Subject<any> = new Subject<any>();
    private _enabledPopup: boolean = false;
    private _blockSetMarker: boolean = false;

    private _markerIcon: { icon: L.Icon<L.IconOptions> } = {
        icon: L.icon({
            iconSize: [25, 41],
            iconAnchor: [10, 41],
            popupAnchor: [2, -40],
            iconUrl: 'https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png',
            shadowUrl: 'https://unpkg.com/leaflet@1.5.1/dist/images/marker-shadow.png'
        })
    };

    constructor(@Inject(BrowserStorageService) private _browserStorageService: BrowserStorageService) {
        // const x: Observable<any> = new Observable<any>();
    }

    public loadExamples(): void {

        const example: Array<MyMarker> = [
            new MyMarker(51.67442355, 7.826039448563509, this.markerIcon),
            new MyMarker(51.67867599152216, 7.809729626319083, this.markerIcon),
            new MyMarker(51.760909341996054, 7.895843529899015, this.markerIcon),
            new MyMarker(51.7628692, 7.9033676, this.markerIcon),
            new MyMarker(-34.356102748242456, 18.473435955902456, this.markerIcon),
            // new MyMarker(39.78737455455555, 2.724314440777316, this.markerIcon),
            // new MyMarker(-34.357150169190845, 18.47397152161124, this.markerIcon),

        ];
        example.forEach(item => {
            this.addMarker(item);
        });
        setTimeout(() => this.enabledPopup = true, 1000);
    }

    public log(log: any): void {
        console.log(log);
    }

    public deleteInList(item: MyMarker): void {
        this.list = this.list.filter(x => x !== item);
        this.removeRouterMarker(item);
    }

    public addMarker(marker: MyMarker): void {
        if (this.blockSetMarker === false) {
            this._list.push(marker);
            this.setMarker.next(marker);
        }
    }

    public addRoutingMarker(marker: &MyMarker): void {
        marker.routing = true;
        // marker.marker.setOpacity(0);
        // marker.marker.options.opacity = 0;
        // this.mapGroup.removeLayer(marker.marker);
        // marker.marker
        this.setRoutingMarker.next(marker);
        setTimeout(() => {
            // console.log(this.map);
        }, 2000)
    }

    public findRouterMarker(item: MyMarker): MyMarker {
        return this.list.find(x => x === item && x.routing);
    }

    public removeRouterMarker(item: &MyMarker): void {
        item.routing = false;
        // item.marker.setOpacity(1);
        // item.marker.options.opacity = 1;
        
        // this.mapGroup.addLayer(item.marker);
        this.setRoutingMarker.next(new MyMarker());
        let counter: number = 0;
        this.list.forEach(x => {
            if (x.routing) {
                counter++;
            }
        });
        if (counter <= 1) {
            this.list.forEach((x: &MyMarker) => {
                if (x.routing) {
                    this.removeRouterMarker(x);
                }
            });
        }
        // this.currentMapRouter
    }

    get currentMapRouter(): L.Routing.Control {
        return this._currentMapRouter;
    }

    set currentMapRouter(value: L.Routing.Control) {
        this._currentMapRouter = value;
    }

    get list(): Array<MyMarker> {
        return this._list;
    }

    set list(value: Array<MyMarker>) {
        this._list = value;
    }

    get goToMarker(): Subject<MyMarker> {
        return this._goToMarker;
    }

    get setMarker(): Subject<MyMarker> {
        return this._setMarker;
    }

    get setRoutingMarker(): Subject<MyMarker> {
        return this._setRoutingMarker;
    }

    get markerIcon(): { icon: L.Icon<L.IconOptions> } {
        return this._markerIcon;
    }

    get reload(): Subject<any> {
        return this._reload;
    }

    get enabledPopup(): boolean {
        return this._enabledPopup;
    }

    set enabledPopup(value: boolean) {
        this._enabledPopup = value;
    }

    get blockSetMarker(): boolean {
        return this._blockSetMarker;
    }

    set blockSetMarker(value: boolean) {
        this._blockSetMarker = value;
    }

    get mapGroup(): L.MarkerClusterGroup {
        return this._mapGroup;
    }

    set mapGroup(value: L.MarkerClusterGroup) {
        this._mapGroup = value;
    }

    get map(): L.Map {
        return this._map;
    }

    set map(value: L.Map) {
        this._map = value;
    }


}
