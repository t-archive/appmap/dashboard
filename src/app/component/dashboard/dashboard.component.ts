import {Component, Inject, OnInit} from '@angular/core';
import {MenuService} from '../../service/menu.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    private _small: boolean;

    constructor(@Inject(MenuService) private _menuService: MenuService) {
    }

    get menuService(): MenuService {
        return this._menuService;
    }

    ngOnInit(): void {
        this.small = (window.innerWidth <= 1000);
    }

    public onResize(event: any): void {
        this.small = (event.target.innerWidth <= 1000);
    }

    get small(): boolean {
        return this._small;
    }

    set small(value: boolean) {
        this._menuService.small = value;
        this._small = value;
    }
}
