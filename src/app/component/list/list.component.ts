import {Component, Inject} from '@angular/core';
import {MarkerService} from '../../service/marker.service';
import {MyMarker} from '../../class/MyMarker';
import {MenuService} from '../../service/menu.service';
import 'leaflet-routing-machine';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent {

    constructor(@Inject(MarkerService) private _markerService: MarkerService,
                @Inject(MenuService) private _menuService: MenuService) {
    }

    get markerService(): MarkerService {
        return this._markerService;
    }

    get menuService(): MenuService {
        return this._menuService;
    }




    public remove(item: &MyMarker): void {
        item.marker.remove();
        this.markerService.deleteInList(item);
        this.markerService.mapGroup.removeLayer(item.marker);
    }

}
