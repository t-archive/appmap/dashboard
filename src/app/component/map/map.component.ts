import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import * as L from 'leaflet';
import {LeafletEvent, LeafletMouseEvent} from 'leaflet';
import {MarkerService} from '../../service/marker.service';
import {MyMarker} from '../../class/MyMarker';
import {GeoSearchControl, OpenStreetMapProvider} from 'leaflet-geosearch';
import 'leaflet.markercluster';
import 'leaflet-routing-machine';
import {MenuService} from '../../service/menu.service';
import {BrowserStorageService} from 'src/app/service/browser-storage.service';
import { throwToolbarMixedModesError } from '@angular/material/toolbar';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewInit {

    private tiles: any = null;
    constructor(@Inject(MarkerService) private _markerService: MarkerService,
                @Inject(BrowserStorageService) private _browserStorageService: BrowserStorageService,
                @Inject(MenuService) private _menuService: MenuService) {
        this.tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        });
    }

    ngOnInit(): void {

    }

    ngAfterViewInit(): void {
        this.initMap();
        // this.initMapGeo();
        this.clickEvent();
        this.searchFunction();
        this.goToService();
        this.setMarkerService();
        this.markerService.loadExamples();
    }

    private initMapGeo(): void {
        navigator.geolocation.getCurrentPosition((location) => {
            this.map.setView({
                lat: location.coords.latitude,
                lng: location.coords.longitude
            }, 12, {animate: true});
        });
    }

    // private async initMap(): Promise<void> {
    private initMap(): void {
        if (!this.map) {
            this.map = L.map('map', {
                center: [51.67441239852524, 7.826029622347606],
                zoom: 10,
                layers: [this.tiles]
            });
        }

        L.Routing.osrmv1({
            language: "de",
            profile: "car"
        });

        this.markerService.currentMapRouter = L.Routing.control({
            waypoints: [],
            show: false,
            waypointMode: 'connect',
            addWaypoints: false,
            autoRoute: true,
            showAlternatives: false,
            fitSelectedRoutes: false,
            pointMarkerStyle: {
                fill: false,
                fillOpacity: 0,
                opacity: 0
            },
            // @ts-ignore
            createMarker: () => null
        });



        // tiles.addTo(this.map);
        this.mapGroup = L.markerClusterGroup({
            chunkedLoading: true,
            spiderfyOnMaxZoom: false,
        });
        this.map.addControl(this.markerService.currentMapRouter);
        this.map.addLayer(this.mapGroup);
        // let t = await this._menuService.openSubject.toPromise();
        // console.log(t);
        this.menuService.openSubject.subscribe((open) => {
            setTimeout(() => {
                this.resizeMap();
                this.goToService();
            });

        });


    }

    private goToService(): void {
        this.markerService.goToMarker.subscribe(item => {
            this.map.setView({
                lat: item.lat,
                lng: item.lng
            }, 16, {animate: true});
            setTimeout(() => {
                if (this.markerService.enabledPopup) {
                    item.marker.openPopup();
                }
            }, 500);
        });
    }

    private searchFunction(): void {
        const searchControl: GeoSearchControl = new GeoSearchControl({
            provider: new OpenStreetMapProvider(),
            style: 'bar',
            showMarker: true, // optional: true|false  - default true
            showPopup: false, // optional: true|false  - default false
            marker: {
                // optional: L.Marker    - default L.Icon.Default
                icon: this.markerService.markerIcon.icon,
                draggable: false
            },
            popupFormat: ({query, result}) => result.label, // optional: function    - default returns result label
            maxMarkers: 1, // optional: number      - default 1
            retainZoomLevel: false, // optional: true|false  - default false
            animateZoom: true, // optional: true|false  - default true
            autoClose: true, // optional: true|false  - default false
            searchLabel: 'Suche...', // optional: string      - default 'Enter address'
            keepResult: false, // optional: true|false  - default false
        });
        this.map.on('geosearch/showlocation', (e: LeafletEvent|any) => {
            console.log(e);
            console.log(e.marker);
            const item: L.Marker = L.marker([e.location.y, e.location.x], this.markerService.markerIcon);
            const myMarker: MyMarker = new MyMarker();
            myMarker.marker = item;
            myMarker.lat = e.location.y;
            myMarker.lng = e.location.x;
            myMarker.description = e.label;
            myMarker.description = e.location.label;
            this.markerService.addMarker(myMarker);
        });
        this.map.addControl(searchControl);
    }

    private clickEvent(): void {
        this.map.on('click', ((e: LeafletMouseEvent) => {
            const item: L.Marker = L.marker([e.latlng.lat, e.latlng.lng], this.markerService.markerIcon);
            const myMarker: MyMarker = new MyMarker();
            myMarker.marker = item;
            myMarker.lat = e.latlng.lat;
            myMarker.lng = e.latlng.lng;
            myMarker.description = '';
            this.markerService.addMarker(myMarker);
        }));
        this.map.on('contextmenu', ((e: LeafletMouseEvent) => {
        }));
    }

    private getNumberOfLeafletObjects(object: any, set: Array<any>) {
        if (object._layers) {
            for (let i in object._layers) {
                this.getNumberOfLeafletObjects(object._layers[i], set);
            }
        } else {
            set.push(object);
        }
    }

    public superFunktion(map: L.Map) {
        let arr: Array<any> = [];
        this.getNumberOfLeafletObjects(map, arr);
        console.log("Hier hasse den Scheiss.", arr);
    }


    private setMarkerService(): void {
        this.markerService.setMarker.subscribe((item) => {
            item.marker.bindPopup(item.description ? item.description : item.lat + '/' + item.lng);
            item.marker.bindTooltip(item.description ? item.description : item.lat + '/' + item.lng);
            item.marker.addTo(this.mapGroup);
            // this.mapGroup.addLayer(item.marker);
            if (this.markerService.enabledPopup) {
                item.marker.openPopup();
            }

            this.findLocation(item);
            this.markerService.reload.subscribe((temp) => {
                item.marker.setPopupContent(item.description ? item.description : item.lat + '/' + item.lng);
                item.marker.setTooltipContent(item.description ? item.description : item.lat + '/' + item.lng);
            });
            item.marker.on('mouseover', () => {
                item.marker.setPopupContent(item.description ? item.description : item.lat + '/' + item.lng);
                item.marker.setTooltipContent(item.description ? item.description : item.lat + '/' + item.lng);
            });
        });


        this.markerService.setRoutingMarker.subscribe(item => {
            const routerItemArray: &L.LatLng[] & L.Routing.Waypoint[] = [];
            this.markerService.list.forEach((myMarker: &MyMarker) => {
                if (myMarker.routing) {
                    myMarker.generateWaypoint();
                    routerItemArray.push(myMarker.waypoint);
                }
            });
            this.superFunktion(this.map);
            this.markerService.currentMapRouter.setWaypoints(routerItemArray);
            this.superFunktion(this.map);
        });
    }

    private findLocation(item: &MyMarker): void {
        if (!item.description) {
            fetch(`https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&langCode=EN&location=${item.lng},${item.lat}`)
            .then(res => res.json())
            .then(myJson => {
                if (myJson && myJson.address && myJson.address.LongLabel) {
                    item.description = myJson.address.LongLabel;
                    this.markerService.reload.next(item.description);
                    // item.description = myJson.address.Match_addr;
                }
            });
        }
    }

    private resizeMap(): void {
        this.map.invalidateSize(true);
        if (!this.menuService.small) {
            this.map.setView(this.markerService.map.getCenter(), this.markerService.map.getZoom());
        }

    }


    get mapGroup(): L.MarkerClusterGroup {
        return this.markerService.mapGroup;
    }

    set mapGroup(value: L.MarkerClusterGroup) {
        this.markerService.mapGroup = value;
    }

    get map(): L.Map {
        return this.markerService.map;
    }

    set map(value: L.Map) {
        this.markerService.map = value;
    }

    get markerService(): MarkerService {
        return this._markerService;
    }

    get menuService(): MenuService {
        return this._menuService;
    }
}
