export function checkIfObjectExists(target: object, key: string): boolean {
    return target.hasOwnProperty(key)
        && typeof target[key] === "object";
}
export function checkIfNumberExists(target: number, key: string): boolean {
    return target.hasOwnProperty(key)
        && typeof target[key] === "number";
}
export function checkIfStringExists(target: string, key: string): boolean {
    return target.hasOwnProperty(key)
        && typeof target[key] === "string";
}
export function checkIfBoolExists(target: boolean, key: string): boolean {
    return target.hasOwnProperty(key)
        && typeof target[key] === "boolean";
}
